<?php

declare(strict_types=1);

namespace App\Services;

use App\Entity\MenuItem;
use App\Entity\MenuItemAdditionalField;
use App\Entity\Order;
use App\Repository\MenuItemAdditionalFieldRepository;
use App\Repository\MenuItemRepository;
use App\Repository\OrderItemsAdditionalFieldRepository;
use App\Repository\OrderItemsRepository;

class OrderService
{
    public function __construct(
        private readonly OrderItemsRepository $orderItemsRepository,
        private readonly OrderItemsAdditionalFieldRepository $orderItemsAdditionalFieldRepository,
        private readonly MenuItemRepository $menuItemRepository,
        private readonly MenuItemAdditionalFieldRepository $menuItemAdditionalFieldRepository
    ) {
    }

    public function getOrderFullData(Order $order): array
    {
        $orders = $order->toArray();
        $orders['menu_items'] = [];
        $orderItems = $this->orderItemsRepository->findBy(['orders' => $order]);

        foreach ($orderItems as $item) {
            $menu_item_data = $this->menuItemRepository->find($item->getMenuItem());
            $menu_item = $item->toArray();
            if ($menu_item_data instanceof MenuItem) {
                $menu_item['basic_price'] = $menu_item_data->getPrice();
                $menu_item['name'] = $menu_item_data->getName();
                $menu_item['image'] = $menu_item_data->getImage();
            }

            $menu_item['additional_fields'] = [];
            $orderItemsAdditionalFields = $this->orderItemsAdditionalFieldRepository->findBy(['order_item' => $item]);

            foreach ($orderItemsAdditionalFields as $field) {
                $additional_field = $this->menuItemAdditionalFieldRepository->find($field->getMenuItemAdditionalField());

                if ($additional_field instanceof MenuItemAdditionalField) {
                    $menu_item_additional_fields = $additional_field->toArrayPres();
                    $menu_item_additional_fields['basic_price'] = $additional_field->getAmount();
                    $menu_item_additional_fields['price'] = $field->getAmount();
                    $menu_item_additional_fields['value'] = $item->getValue();
                    $menu_item['additional_fields'][] = $menu_item_additional_fields;
                }
            }

            $orders['menu_items'][] = $menu_item;
        }

        return $orders;
    }
}

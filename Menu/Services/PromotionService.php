<?php

declare(strict_types=1);

/**
 * Menu SomeCode.
 *
 * @author Paweł Liwocha PAWELDESIGN <pawel.liwocha@gmail.com>
 * @copyright Copyright (c) 2023  Paweł Liwocha PAWELDESIGN (https://paweldesign.com)
 */

namespace App\Services;

use App\Entity\MenuItem;
use App\Entity\Promotion;
use App\Enum\PromotionType;
use App\Repository\PromotionRepository;

class PromotionService
{
    public function __construct(
        private readonly PromotionRepository $promotionRepository
    ) {
    }

    public function calculatePrice(MenuItem $menuItem): float|int|null
    {
        $promotion = $this->promotionRepository->getActivePromotionForMenuItem($menuItem);

        if ($promotion instanceof Promotion) {
            if ($promotion->getPromotionType() === PromotionType::Amount) {
                $promoPrice = $menuItem->getPrice() - $promotion->getValue();

                if ($promoPrice < 0) {
                    return 0;
                }

                return $promoPrice;
            }

            $percent = $promotion->getValue() / 100;

            return $menuItem->getPrice() - ($menuItem->getPrice() * $percent);
        }

        return null;
    }
}

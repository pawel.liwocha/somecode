<?php

declare(strict_types=1);

/**
 * Menu SomeCode.
 *
 * @author Paweł Liwocha PAWELDESIGN <pawel.liwocha@gmail.com>
 * @copyright Copyright (c) 2023  Paweł Liwocha PAWELDESIGN (https://paweldesign.com)
 */

namespace App\Controller\Api;

use App\Entity\MenuItem;
use App\Entity\MenuItemAdditionalField;
use App\Entity\Order;
use App\Entity\OrderItems;
use App\Entity\OrderItemsAdditionalField;
use App\Entity\Restaurant;
use App\Entity\RestaurantTable;
use App\Entity\User;
use App\Enum\OrderStatus;
use App\Enum\OrderType;
use App\Enum\PaymentType;
use App\Repository\MenuItemAdditionalFieldRepository;
use App\Repository\MenuItemRepository;
use App\Repository\RestaurantTableRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route(path: '/order', name: 'order_')]
class OrderController extends AbstractController
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly LoggerInterface $logger,
        private readonly RestaurantTableRepository $restaurantTableRepository,
        private readonly UserRepository $userRepository,
        private readonly MenuItemRepository $menuItemRepository,
        private readonly MenuItemAdditionalFieldRepository $menuItemAdditionalFieldRepository
    ) {
    }

    #[Route(path: '/add', name: 'add', methods: ['POST'])]
    public function add(Request $request): JsonResponse
    {
        $content = $request->request->all();

        if (
            isset($content['restaurant_table_uuid'])
            && $content['restaurant_table_uuid'] !== ''
            && isset($content['amount'])
            && $content['amount'] !== ''
            && isset($content['payment_type'])
            && $content['payment_type'] !== ''
            && isset($content['order_type'])
            && $content['order_type'] !== ''
            && isset($content['menu_items'])
            && $content['menu_items'] !== ''
        ) {
            $restaurantTable = $this->restaurantTableRepository->findOneBy(['uuid' => htmlspecialchars(trim((string) $content['restaurant_table_uuid'])), 'deleted' => 0]);
            $amountInput = (int) $content['amount'];
            $feeInput = isset($content['fee']) ? (int) $content['fee'] : 0;
            $paymentTypeInput = htmlspecialchars(trim((string) $content['payment_type'])); // CASH : CARD : ONLINE
            $orderTypeInput = htmlspecialchars(trim((string) $content['order_type'])); // ONSITE : TAKEAWAY : DELIVERY
            $commentInput = isset($content['comment']) ? htmlspecialchars(trim((string) $content['comment'])) : null;
            $menuItemsInput = (array) json_decode((string) $content['menu_items'], true);
            /* json array:
                [
                    {
                        "uuid": "uuid menu item",
                        "value": value of ordered products,
                        "comment": "string Comment",
                        "additional_fields": [
                            {
                                "uuid": "UUID selected additional field"
                            }, {
                                "uuid": "UUID selected additional field"
                            }
                        ]
                    }, {
                        "uuid": "uuid menu item",
                        "value": value of ordered products,
                        "comment": "string Comment",
                        "additional_fields": [
                            {
                                "uuid": "UUID selected additional field"
                            }
                        ]
                    }
                ]
            */
            $userInput = isset($content['user_uuid']) ? htmlspecialchars(trim((string) $content['user_uuid'])) : null;
            $user = $userInput ? $this->userRepository->findOneBy(['uuid' => $userInput]) : null;

            $paymentTypeIn = PaymentType::Cash;
            foreach (PaymentType::cases() as $type) {
                if ($type->value === $paymentTypeInput) {
                    $paymentTypeIn = $type;
                }
            }

            $orderTypeIn = OrderType::OnSite;
            foreach (OrderType::cases() as $type) {
                if ($type->value === $orderTypeInput) {
                    $orderTypeIn = $type;
                }
            }

            if ($menuItemsInput === []) {
                return $this->json(['Menu items are empty'], Response::HTTP_BAD_REQUEST);
            }

            if (
                $restaurantTable instanceof RestaurantTable
                && $paymentTypeInput !== ''
                && $orderTypeInput !== ''
                && $amountInput >= 0
            ) {
                $order = new Order();
                $order->setUser($user);
                $order->setRestaurantTable($restaurantTable);
                $order->setAmount($amountInput);
                $order->setFee($feeInput);
                $order->setDiscountCode(null);
                $order->setDiscountAmount(null);
                $order->setPaymentType($paymentTypeIn);
                $order->setOrderType($orderTypeIn);
                $order->setStatus(OrderStatus::Created);
                $order->setComment($commentInput);
                $order->setDeleted(0);

                $amountSum = 0;

                try {
                    $this->entityManager->persist($order);
                    $this->entityManager->flush();
                } catch (\Exception $exception) {
                    $this->logger->error('Add new Order error: '.$exception->getMessage().PHP_EOL.' RestaurantTable: '.$restaurantTable->getUuid().'; User: '.$user?->getUuid().'; Amount: '.$amountInput.PHP_EOL);

                    return $this->json(['Error when save new main order'], Response::HTTP_BAD_REQUEST);
                }

                $orderItemsReturn = [];

                try {
                    foreach ($menuItemsInput as $value) {
                        $menuItem = $this->menuItemRepository->findOneBy(['uuid' => $value['uuid']]);
                        if (!$menuItem instanceof MenuItem) {
                            $this->logger->error('Add new Menu item to Order error; Menu item: '.$value['uuid'].' RestaurantTable: '.$restaurantTable->getUuid().'; User: '.$user?->getUuid().PHP_EOL);

                            return $this->json(['Not find menu item by uuid'], Response::HTTP_BAD_REQUEST);
                        }

                        $menuItemComment = isset($value['comment']) ? htmlspecialchars(trim((string) $value['comment'])) : null;
                        $valueOfMenuItems = isset($value['value']) ? (int) $value['value'] : 1;
                        $priceMenuItem = $menuItem->getPrice() * $valueOfMenuItems;
                        $amountSum += $priceMenuItem;

                        $orderItem = new OrderItems();
                        $orderItem->setOrder($order);
                        $orderItem->setMenuItem($menuItem);
                        $orderItem->setValue($valueOfMenuItems);
                        $orderItem->setAmount($priceMenuItem);
                        $orderItem->setComment($menuItemComment);
                        $orderItem->setDeleted(0);

                        $this->entityManager->persist($orderItem);
                        $this->entityManager->flush();

                        $returnMenuItem = $orderItem->toArray();
                        $returnMenuItem['additional_fields'] = [];

                        foreach ((array) $value['additional_fields'] as $val) {
                            $menuItemAdditionalField = $this->menuItemAdditionalFieldRepository->findOneBy(['uuid' => $val['uuid']]);

                            if (!$menuItemAdditionalField instanceof MenuItemAdditionalField) {
                                $this->logger->error('Add new Menu item Additional field to Order error; Additional Field: '.$val['uuid'].' RestaurantTable: '.$restaurantTable->getUuid().'; User: '.$user?->getUuid().PHP_EOL);

                                return $this->json(['Not find menu item additional field by uuid'], Response::HTTP_BAD_REQUEST);
                            }

                            $priceMenuItemAdditionalField = $menuItemAdditionalField->getAmount() * $valueOfMenuItems;

                            $orderItemAdditionalField = new OrderItemsAdditionalField();
                            $orderItemAdditionalField->setOrderItem($orderItem);
                            $orderItemAdditionalField->setMenuItemAdditionalField($menuItemAdditionalField);
                            $orderItemAdditionalField->setAmount($priceMenuItemAdditionalField);
                            $orderItemAdditionalField->setDeleted(0);

                            $amountSum += $priceMenuItemAdditionalField;
                            $orderItem->setAmount($orderItem->getAmount() + $priceMenuItemAdditionalField);

                            $this->entityManager->persist($orderItemAdditionalField);
                            $returnMenuItem['additional_fields'][] = $orderItemAdditionalField->toArray();
                        }

                        $orderItemsReturn[] = $returnMenuItem;

                        $this->entityManager->flush();
                    }

                    $order->setAmount($amountSum);

                    $this->entityManager->persist($order);
                    $this->entityManager->flush();
                } catch (\Exception $exception) {
                    $this->logger->error('Add new Order error: '.$exception->getMessage().PHP_EOL.' RestaurantTable: '.$restaurantTable->getUuid().'; User: '.$user?->getUuid().'; Amount: '.$amountInput.PHP_EOL);

                    return $this->json(['Error when save new order items'], Response::HTTP_BAD_REQUEST);
                }

                if ($amountSum !== $amountInput) {
                    return $this->json(['Sent price is not equals as calculate'], Response::HTTP_CONFLICT);
                }

                $orderReturn = $order->toArray();
                $orderReturn['menu_items'] = $orderItemsReturn;

                return $this->json($orderReturn, Response::HTTP_OK);
            }

            return $this->json(['Required params was incorrect'], Response::HTTP_CONFLICT);
        }

        return $this->json(['Required params was empty'], Response::HTTP_BAD_REQUEST);
    }
}

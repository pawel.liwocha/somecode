<?php

namespace App\Infrastructure\Przelewy24;

use App\Infrastructure\Przelewy24\Api\Api;
use App\Infrastructure\Przelewy24\Api\Request\ApiRequest;

class Transaction extends ApiRequest
{
    final public const LANGUAGE_PL = 'pl';

    final public const LANGUAGE_EN = 'en';

    final public const LANGUAGE_DE = 'de';

    final public const LANGUAGE_ES = 'es';

    final public const LANGUAGE_IT = 'it';

    final public const CHANNEL_CARD = 1;

    final public const CHANNEL_WIRE = 2;

    final public const CHANNEL_TRADITIONAL_WIRE = 4;

    final public const CHANNEL_ALL_24_7 = 16;

    final public const CHANNEL_PREPAYMENT = 32;

    final public const CHANNEL_PAY_BY_LINK = 64;

    final public const ENCODING_ISO_8859_2 = 'ISO-8859-2';

    final public const ENCODING_UTF_8 = 'UTF-8';

    final public const ENCODING_WINDOWS_1250 = 'Windows-1250';

    // private array|TransactionProduct[] $products = [];

    protected array $signatureAttributes = [
        'session_id',
        'merchant_id',
        'amount',
        'currency',
        'crc',
    ];

    protected array $signatureVerifyAttributes = [
        'session_id',
        'order_id',
        'amount',
        'currency',
        'crc',
    ];

    public function __construct(array $parameters = [])
    {
        $this->parameters = array_merge([
            'currency' => 'PLN',
            'language' => self::LANGUAGE_PL,
            'encoding' => self::ENCODING_UTF_8,
            'api_version' => Api::VERSION,
        ], $parameters);
    }

    public function addProduct(array $product): self
    {
        // $this->products[] = new TransactionProduct($product);

        return $this;
    }
}

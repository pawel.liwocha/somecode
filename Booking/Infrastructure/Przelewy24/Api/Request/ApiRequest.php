<?php

namespace App\Infrastructure\Przelewy24\Api\Request;

use App\Infrastructure\Przelewy24\Config;

class ApiRequest implements SignedApiRequest
{
    protected array $parameters = [];

    protected array $signatureAttributes = [];

    protected array $signatureVerifyAttributes = [];

    public function setConfig(Config $config): SignedApiRequest
    {
        $this->parameters = array_merge($this->parameters, $config->toArray());

        return $this;
    }

    public function parameters(): array
    {
        $parameters = [];

        foreach ($this->parameters as $parameter => $value) {
            $parameters['p24_'.$parameter] = $value;
        }

        $parameters['p24_sign'] = $this->signature();

        return $parameters;
    }

    public function signature(): string
    {
        $parameters = [];

        foreach ($this->signatureAttributes as $param) {
            $parameters[$param] = $this->parameters[$param];
        }

        return md5(
            implode('|', $parameters)
        );
    }

    public function parametersHash(): array
    {
        $parameters = [];

        foreach ($this->parameters as $parameter => $value) {
            $parameters['p24_'.$parameter] = $value;
        }

        // $parameters['p24_sign'] = $this->signatureHash();

        return $parameters;
    }

    public function signatureHash(): string
    {
        $parameters = [];

        foreach ($this->signatureVerifyAttributes as $param) {
            $parameters[$param] = $this->parameters[$param];
        }

        return hash('sha384', (string) json_encode($parameters, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
    }

    public function __get(string $name): string
    {
        return $this->parameters[$name];
    }

    public function __set(string $name, string $value): void
    {
        $this->parameters[$name] = $value;
    }
}

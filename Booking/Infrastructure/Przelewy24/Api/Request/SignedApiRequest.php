<?php

namespace App\Infrastructure\Przelewy24\Api\Request;

use App\Infrastructure\Przelewy24\Config;

interface SignedApiRequest
{
    public function setConfig(Config $config): self;

    public function parameters(): array;

    public function parametersHash(): array;

    public function signature(): string;
}

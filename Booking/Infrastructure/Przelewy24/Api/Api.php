<?php

namespace App\Infrastructure\Przelewy24\Api;

use App\Infrastructure\Przelewy24\Api\Request\SignedApiRequest;
use App\Infrastructure\Przelewy24\Api\Response\RegisterTransactionResponse;
use App\Infrastructure\Przelewy24\Api\Response\TestConnectionResponse;
use App\Infrastructure\Przelewy24\Api\Response\VerifyTransactionResponse;
use App\Infrastructure\Przelewy24\Config;
use App\Infrastructure\Przelewy24\Exceptions\ApiResponseException;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\RequestOptions;
use Psr\Http\Message\ResponseInterface;

class Api
{
    final public const VERSION = '3.2';

    final public const URL_LIVE = 'https://secure.przelewy24.pl/';

    final public const URL_SANDBOX = 'https://sandbox.przelewy24.pl/';

    final public const ENDPOINT_TEST = 'testConnection';

    final public const ENDPOINT_REGISTER = 'trnRegister';

    final public const ENDPOINT_VERIFY = 'trnVerify';

    final public const ENDPOINT_PAYMENT_GATEWAY = 'trnRequest';

    private readonly ClientInterface $client;

    public function __construct(private readonly Config $config)
    {
        $this->client = new GuzzleClient([
            'base_uri' => $this->getApiUrl(),
            RequestOptions::CONNECT_TIMEOUT => 10,
            RequestOptions::TIMEOUT => 30,
        ]);
    }

    /**
     * @throws ApiResponseException
     */
    public function testConnection(): TestConnectionResponse
    {
        $response = $this->request(self::ENDPOINT_TEST, [
            'p24_merchant_id' => $this->config->getMerchantId(),
            'p24_pos_id' => $this->config->getPosId(),
            'p24_sign' => md5($this->config->getPosId().'|'.$this->config->getCrc()),
        ]);

        return new TestConnectionResponse($response);
    }

    /**
     * @throws ApiResponseException
     */
    public function registerTransaction(SignedApiRequest $apiRequest): RegisterTransactionResponse
    {
        $apiRequest->setConfig($this->config);

        $response = new RegisterTransactionResponse(
            $this->request(self::ENDPOINT_REGISTER, $apiRequest->parameters())
        );

        $response->setGatewayUrl(
            $this->getApiUrl().self::ENDPOINT_PAYMENT_GATEWAY
        );

        return $response;
    }

    /**
     * @throws ApiResponseException
     */
    public function verifyTransaction(SignedApiRequest $apiRequest): VerifyTransactionResponse
    {
        $apiRequest->setConfig($this->config);

        return new VerifyTransactionResponse(
            $this->request(self::ENDPOINT_VERIFY, $apiRequest->parametersHash())
        );
    }

    private function client(): ClientInterface
    {
        return $this->client;
    }

    private function request(string $endpoint, array $parameters): ResponseInterface
    {
        return $this->client()->post($endpoint, [
            'form_params' => $parameters,
        ]);
    }

    private function getApiUrl(): string
    {
        return $this->config->isLiveMode() ? self::URL_LIVE : self::URL_SANDBOX;
    }
}

<?php

namespace App\Infrastructure\Przelewy24\Api\Response;

use App\Infrastructure\Przelewy24\Exceptions\ApiResponseException;
use Psr\Http\Message\ResponseInterface;

abstract class ApiResponse
{
    protected string $error;

    protected string $errorMessage = '';

    /**
     * @throws ApiResponseException
     */
    public function __construct(ResponseInterface $response)
    {
        $contents = $response->getBody();

        parse_str($contents, $parameters);

        foreach ($parameters as $key => $value) {
            if (property_exists($this, (string) $key)) {
                $this->$key = $value;
            }
        }

        if ($this->hasError()) {
            throw new ApiResponseException($this->getError());
        }
    }

    protected function hasError(): bool
    {
        return !empty($this->error);
    }

    protected function getError(): string
    {
        return $this->error.' - '.$this->errorMessage;
    }
}

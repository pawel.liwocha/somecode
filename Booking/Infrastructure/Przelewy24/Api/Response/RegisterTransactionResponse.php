<?php

namespace App\Infrastructure\Przelewy24\Api\Response;

class RegisterTransactionResponse extends ApiResponse
{
    protected ?string $token = null;

    private ?string $gatewayUrl = null;

    public function token(): ?string
    {
        return $this->token;
    }

    public function setGatewayUrl(string $url): self
    {
        $this->gatewayUrl = $url;

        return $this;
    }

    public function redirectUrl(): string
    {
        return $this->gatewayUrl.'/'.$this->token;
    }
}

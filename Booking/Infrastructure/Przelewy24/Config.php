<?php

namespace App\Infrastructure\Przelewy24;

class Config
{
    private int $merchantId;

    private int $posId;

    private string $crc;

    private bool $isLiveMode;

    public function __construct(array $parameters)
    {
        $this->set($parameters);
    }

    public function set(array $parameters): self
    {
        if (!$parameters['merchant_id']) {
            throw new \InvalidArgumentException('"merchant_id" must be specified in the configuration parameters.');
        }

        if (!$parameters['pos_id']) {
            throw new \InvalidArgumentException('"pos_id" must be specified in the configuration parameters.');
        }

        if (!$parameters['crc']) {
            throw new \InvalidArgumentException('"crc" must be specified in the configuration parameters.');
        }

        $live = $parameters['live'] && filter_var($parameters['live'], FILTER_VALIDATE_BOOLEAN);

        $this->merchantId = $parameters['merchant_id'];
        $this->posId = $parameters['pos_id'];
        $this->crc = $parameters['crc'];
        $this->isLiveMode = $live;

        return $this;
    }

    public function getMerchantId(): int
    {
        return $this->merchantId;
    }

    public function getPosId(): int
    {
        return $this->posId;
    }

    public function getCrc(): string
    {
        return $this->crc;
    }

    public function isLiveMode(): bool
    {
        return $this->isLiveMode;
    }

    /**
     * @return array{merchant_id: int, pos_id: int, crc: string}
     */
    public function toArray(): array
    {
        return [
            'merchant_id' => $this->getMerchantId(),
            'pos_id' => $this->getPosId(),
            'crc' => $this->getCrc(),
        ];
    }
}

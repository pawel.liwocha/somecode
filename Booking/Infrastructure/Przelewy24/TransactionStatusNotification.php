<?php

namespace App\Infrastructure\Przelewy24;

class TransactionStatusNotification
{
    public function __construct(private readonly array $parameters)
    {
    }

    public function sessionId(): string
    {
        return $this->parameters['p24_session_id'];
    }

    public function amount(): int
    {
        return $this->parameters['p24_amount'];
    }

    public function currency(): string
    {
        return $this->parameters['p24_currency'];
    }

    public function orderId(): int
    {
        return $this->parameters['p24_order_id'];
    }

    public function method(): string
    {
        return $this->parameters['p24_method'];
    }

    public function statement(): string
    {
        return $this->parameters['p24_statement'];
    }

    public function sign(): string
    {
        return $this->parameters['p24_sign'];
    }

    public function toArray(): array
    {
        return $this->parameters;
    }
}

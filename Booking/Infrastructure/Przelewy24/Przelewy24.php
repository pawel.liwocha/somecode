<?php

namespace App\Infrastructure\Przelewy24;

use App\Infrastructure\Przelewy24\Api\Api;
use App\Infrastructure\Przelewy24\Api\Response\RegisterTransactionResponse;
use App\Infrastructure\Przelewy24\Api\Response\TestConnectionResponse;
use App\Infrastructure\Przelewy24\Api\Response\VerifyTransactionResponse;
use App\Infrastructure\Przelewy24\Exceptions\ApiResponseException;

class Przelewy24
{
    private readonly Config $config;

    private readonly Api $api;

    public function __construct(array $parameters)
    {
        $this->config = new Config($parameters);
        $this->api = new Api($this->config);
    }

    /**
     * @throws ApiResponseException
     */
    public function testConnection(): TestConnectionResponse
    {
        return $this->api->testConnection();
    }

    /**
     * @throws ApiResponseException
     */
    public function transaction(array|Transaction $transaction): RegisterTransactionResponse
    {
        if (is_array($transaction)) {
            $transaction = new Transaction($transaction);
        }

        return $this->api->registerTransaction($transaction);
    }

    /**
     * @throws ApiResponseException
     */
    public function verify(TransactionVerification|array $verification): VerifyTransactionResponse
    {
        if (is_array($verification)) {
            $verification = new TransactionVerification($verification);
        }

        return $this->api->verifyTransaction($verification);
    }

    public function handleWebhook(): TransactionStatusNotification
    {
        parse_str((string) file_get_contents('php://input'), $data);

        return new TransactionStatusNotification($data);
    }
}

<?php

namespace App\Infrastructure\Przelewy24;

use App\Infrastructure\Przelewy24\Api\Request\ApiRequest;

class TransactionVerification extends ApiRequest
{
    protected array $signatureAttributes = [
        'session_id',
        'merchant_id',
        'amount',
        'currency',
        'crc',
    ];

    protected array $signatureVerifyAttributes = [
        'session_id',
        'order_id',
        'amount',
        'currency',
        'crc',
    ];

    public function __construct(array $parameters = [])
    {
        $this->parameters = array_merge([
            'currency' => 'PLN',
        ], $parameters);
    }
}

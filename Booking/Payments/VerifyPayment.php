<?php

declare(strict_types=1);

namespace App\Payments;

use App\Entity\Payment;
use App\Entity\Reservation;
use App\Infrastructure\Przelewy24\Api\Response\VerifyTransactionResponse;
use App\Infrastructure\Przelewy24\Przelewy24;

class VerifyPayment
{
    private readonly Przelewy24 $przelewy24;

    private readonly int $przelewy24MerchantId;

    private readonly string $przelewy24CRC;

    public function __construct(
        string $przelewy24Live,
        int $przelewy24MerchantId,
        string $przelewy24CRC,
        int $przelewy24PosId,
    ) {
        $this->przelewy24 = new Przelewy24([
            'merchant_id' => $przelewy24MerchantId,
            'pos_id' => $przelewy24PosId,
            'crc' => $przelewy24CRC,
            'live' => $przelewy24Live,
        ]);

        $this->przelewy24MerchantId = $przelewy24MerchantId;
        $this->przelewy24CRC = $przelewy24CRC;
    }

    public function verify(Payment $payment, Reservation $reservation): VerifyTransactionResponse
    {
        return $this->przelewy24->verify([
            'session_id' => $reservation->getUuid(),
            'merchant_id' => $this->przelewy24MerchantId,
            'order_id' => (int) $payment->getPrzelewyFinalOrderId(),
            'amount' => $payment->getAmount(),
            'currency' => $payment->getCurrency(),
            'crc' => $this->przelewy24CRC,
            'sign' => $payment->getSign(),
        ]);
    }
}

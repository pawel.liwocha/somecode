<?php

declare(strict_types=1);

namespace App\Payments;

use App\Entity\Payment;
use App\Entity\Reservation;
use App\Enum\PaymentStatus;
use App\Infrastructure\Przelewy24\Api\Response\RegisterTransactionResponse;
use App\Infrastructure\Przelewy24\Exceptions\Przelewy24Exception;
use App\Infrastructure\Przelewy24\Przelewy24;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class CreatePayment
{
    private readonly Przelewy24 $przelewy24;

    public function __construct(
        string $przelewy24Live,
        int $przelewy24MerchantId,
        string $przelewy24CRC,
        int $przelewy24PosId,
        private readonly string $przelewy24UrlReturn,
        private readonly string $przelewy24UrlWebhook,
        private readonly EntityManagerInterface $entityManager,
        private readonly LoggerInterface $logger
    ) {
        $this->przelewy24 = new Przelewy24([
            'merchant_id' => $przelewy24MerchantId,
            'pos_id' => $przelewy24PosId,
            'crc' => $przelewy24CRC,
            'live' => $przelewy24Live,
        ]);
    }

    public function create(Payment $payment, Reservation $reservation): RegisterTransactionResponse|bool
    {
        try {
            $transaction = $this->przelewy24->transaction([
                'session_id' => $reservation->getUuid(),
                'url_return' => $this->przelewy24UrlReturn.$reservation->getUuid(),
                'url_status' => $this->przelewy24UrlWebhook.$reservation->getUuid(),
                'amount' => $payment->getAmount(),
                'description' => 'Reservation uuid: '.$reservation->getUuid().', License plate: '.$reservation->getLicensePlate().', User: '.$reservation->getFirstName().' '.$reservation->getLastName().', People number: '.$reservation->getPeopleNumber().', Cars number: '.$reservation->getCarNumber(),
                'email' => $reservation->getEmail(),
            ]);
        } catch (Przelewy24Exception $przelewy24Exception) {
            $this->logger->error('Error: create transaction: '.$przelewy24Exception->getMessage());
            throw new Przelewy24Exception($przelewy24Exception->getMessage());
        }

        if ($transaction->token()) {
            $payment->setPrzelewyOrderId($transaction->token());
            $payment->setPaymentStatus(PaymentStatus::InRealization);
            $payment->setUpdatedAt(new \DateTimeImmutable('now'));
            $this->entityManager->persist($payment);

            $this->entityManager->flush();

            return $transaction;
        }

        return false;
    }
}

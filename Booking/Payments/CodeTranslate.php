<?php

declare(strict_types=1);

namespace App\Payments;

abstract class CodeTranslate
{
    final public const CODE = '-1';

    /**
     * Get helper array of available codes.
     */
    public static function getCodes(): array
    {
        $codes = [];

        $reflection = new \ReflectionClass(static::class);

        foreach ($reflection->getConstants() as $key => $value) {
            $codes[$key] = $value;
        }

        return $codes;
    }

    /**
     * Get Code for searching value.
     */
    public static function getCode(string $name, string $default_value): string
    {
        $codes = static::getCodes();

        if (in_array($name, $codes)) {
            return $name;
        }

        $name = mb_strtoupper(trim($name));
        if (array_key_exists($name, $codes)) {
            return $codes[$name];
        }

        return $default_value;
    }
}

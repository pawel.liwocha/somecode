<?php

declare(strict_types=1);

namespace App\Payments;

class ErrorCode extends CodeTranslate
{
    final public const ERR00 = 'err00';

    final public const ERR01 = 'err01';

    final public const ERR02 = 'err02';

    final public const ERR03 = 'err03';

    final public const ERR04 = 'err04';

    final public const ERR05 = 'err05';

    final public const ERR06 = 'err06';

    final public const ERR07 = 'err07';

    final public const ERR08 = 'err08';

    final public const ERR09 = 'err09';

    final public const ERR10 = 'err10';

    final public const ERR49 = 'err49';

    final public const ERR51 = 'err51';

    final public const ERR52 = 'err52';

    final public const ERR53 = 'err53';

    final public const ERR54 = 'err54';

    final public const ERR55 = 'err55';

    final public const ERR56 = 'err56';

    final public const ERR57 = 'err57';

    final public const ERR58 = 'err58';

    final public const ERR101 = 'err101';

    final public const ERR102 = 'err102';

    final public const ERR103 = 'err103';

    final public const ERR104 = 'err104';

    final public const ERR105 = 'err105';

    final public const ERR106 = 'err106';

    final public const ERR161 = 'err161';

    final public const ERR162 = 'err162';

    /**
     * List of error description.
     */
    protected static array $error_description = [
        self::ERR00 => 'Incorrect call',
        self::ERR01 => 'Authorization answer confirmation was not received.',
        self::ERR02 => 'Authorization answer was not received.',
        self::ERR03 => 'This query has been already processed.',
        self::ERR04 => 'Authorization query incomplete or incorrect.',
        self::ERR05 => 'Store configuration cannot be read.',
        self::ERR06 => 'Saving of authorization query failed.',
        self::ERR07 => 'Another payment is being concluded.',
        self::ERR08 => 'Undetermined store connection status.',
        self::ERR09 => 'Permitted corrections amount has been exceeded.',
        self::ERR10 => 'Incorrect transaction value!',
        self::ERR49 => 'To high transaction risk factor.',
        self::ERR51 => 'Incorrect reference method.',
        self::ERR52 => 'Incorrect feedback on session information!',
        self::ERR53 => 'Transaction error !',
        self::ERR54 => 'Incorrect transaction value!',
        self::ERR55 => 'Incorrect transaction id!',
        self::ERR56 => 'Incorrect card',
        self::ERR57 => 'Incompatibility of TEST flag !',
        self::ERR58 => 'Incorrect sequence number !',
        self::ERR101 => 'Incorrect call.',
        self::ERR102 => 'Allowed transaction time has expired .',
        self::ERR103 => 'Incorrect transfer value.',
        self::ERR104 => 'Transaction awaits confirmation.',
        self::ERR105 => 'Transaction finished after allowed time.',
        self::ERR106 => 'Transaction result verification',
        self::ERR161 => 'Transaction request terminated by user.',
        self::ERR162 => 'Transaction request terminated by user.',
    ];

    /**
     * Get description of error from transfers24.
     */
    public static function getDescription(string $name): ?string
    {
        $codes = static::getCodes();

        if (in_array($name, $codes)) {
            return self::$error_description[$name];
        }

        return null;
    }
}

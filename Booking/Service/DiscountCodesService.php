<?php

namespace App\Service;

use App\Entity\DiscountCodes;
use App\Entity\Parking;
use App\Enum\DiscountCodesStatus;
use App\Repository\DiscountCodesRepository;
use App\Repository\DiscountCodesUsedRepository;
use Doctrine\Common\Collections\Order;
use Doctrine\ORM\EntityManagerInterface;

class DiscountCodesService
{
    public function __construct(
        private readonly DiscountCodesRepository $discountCodesRepository,
        private readonly DiscountCodesUsedRepository $discountCodesUsedRepository,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function checkDiscountCode(string $name, Parking $parking): bool|DiscountCodes
    {
        $date = new \DateTimeImmutable('now');

        $code = $this->discountCodesRepository->findOneBy([
            'name' => $name,
            'parking' => $parking,
            'status' => DiscountCodesStatus::Active,
        ], ['valid_from' => Order::Descending->value]);

        if (!$code instanceof DiscountCodes) {
            return false;
        }

        if ($code->getValidFrom() >= $date) {
            return false;
        }

        if ($code->getValidTo() instanceof \DateTimeImmutable && $code->getValidTo() <= $date) {
            return false;
        }

        $countUsedCode = $this->discountCodesUsedRepository->getCountOfUsedByCode($code);

        if ($countUsedCode >= $code->getLots()) {
            $code->setStatus(DiscountCodesStatus::Inactive);
            $code->setStatusDesc('Zużyto wszystkie kody');

            $this->entityManager->persist($code);
            $this->entityManager->flush();

            return false;
        }

        return $code;
    }
}

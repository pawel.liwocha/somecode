<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\DiscountCodes;
use App\Entity\Parking;
use App\Entity\TimeRangeDiscount;
use App\Repository\ParkingPriceRangeRepository;
use Doctrine\Common\Collections\Order;

class CalculatePriceService
{
    public function __construct(
        private readonly ParkingPriceRangeRepository $parkingPriceRangeRepository,
        private readonly TimeRangeDiscountService $timeRangeDiscountService
    ) {
    }

    public function calculate(\DateTimeImmutable $dateFrom, \DateTimeImmutable $dateTo, Parking $parking, int $carNumber, DiscountCodes $discountCode = null): int
    {
        $ranges = $this->parkingPriceRangeRepository->findBy(['parking' => $parking, 'deleted' => 0], ['day_from' => Order::Ascending->value]);
        $numberOfDays = (int) $dateTo->diff($dateFrom)->format('%a');

        $price = null;
        $lastPrice = null;
        $days = 0;

        foreach ($ranges as $range) {
            $lastPrice = (int) $range->getPrice();
            ++$days;
            if (($range->getDayFrom() <= $numberOfDays) && ($numberOfDays <= $range->getDayTo())) {
                $price = $range->getPrice();
                break;
            }
        }

        if ($price === null) {
            $daysToAdd = $numberOfDays - $days;
            $priceToAdd = $parking->getBasicPrice() !== null ? $daysToAdd * $parking->getBasicPrice() : $daysToAdd * 1000;

            $price = $lastPrice + $priceToAdd;
        }

        $price *= $carNumber;

        if ($discountCode instanceof DiscountCodes) {
            $price -= $discountCode->getValue();
        }

        $timeRangeDiscount = $this->timeRangeDiscountService->getTimeRangeDiscount($dateFrom);

        if ($timeRangeDiscount instanceof TimeRangeDiscount) {
            $price -= $timeRangeDiscount->getValue();
        }

        return $price;
    }
}
